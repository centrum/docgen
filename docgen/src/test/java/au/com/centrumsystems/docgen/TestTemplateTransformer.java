package au.com.centrumsystems.docgen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import junit.framework.TestCase;

import org.apache.commons.io.FileUtils;
import org.bouncycastle.util.encoders.Base64;
import org.docx4j.openpackaging.exceptions.Docx4JException;

/**
 * Unit test for simple App.
 */
public class TestTemplateTransformer extends TestCase {

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		Logger logger = Logger.getLogger("au.com.centrumsystems.docgen.TemplateTransformer");
		Handler handler = new ConsoleHandler();
		handler.setLevel(Level.ALL);
		logger.addHandler(handler);
		logger.setLevel(Level.ALL);
		logger.setUseParentHandlers(false);
	}

	private String readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}

	public void testGeneratingAWrodDocumentUsingEncodedString() throws IOException, Docx4JException {

		final URL dataFileURL = this.getClass().getClassLoader().getResource("invoice-data2.xml");
		final URL templateFile = this.getClass().getClassLoader().getResource("invoice-template2.docx");
		final String xmlData = readFile(dataFileURL.getFile());
		final byte[] template = FileUtils.readFileToByteArray(new File(templateFile.getFile()));
		final byte[] encodedTemplate = Base64.encode(template);

		TemplateTransformer transformer = new TemplateTransformer();

		String responseData = transformer.generateDocumentAndSaveAsWord(new String(encodedTemplate), xmlData);
		assertNotNull(responseData);
	}

	public void testGeneratingAWordDocumentUsingByteArrays() throws IOException, Docx4JException {
		File tempFile = null;
		try {
			URL dataFileURL = this.getClass().getClassLoader().getResource("invoice-data2.xml");
			URL templateFile = this.getClass().getClassLoader().getResource("invoice-template2.docx");
			tempFile = File.createTempFile("gen", "docx");

			final byte[] template = FileUtils.readFileToByteArray(new File(templateFile.getFile()));
			String xmlData = readFile(dataFileURL.getFile());

			TemplateTransformer transformer = new TemplateTransformer();
			transformer.generateDocumentAndSaveAsWord(template, xmlData, tempFile);

			assertTrue(tempFile.exists() && tempFile.length() > 0L);

		} finally {
			if (tempFile != null) {
				tempFile.delete();
			}
		}
	}
}
