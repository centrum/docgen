package au.com.centrumsystems.docgen;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.Docx4J;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

/**
 * Created by AlexBullen on 17/10/2014.
 */
public class TemplateTransformer {
	/* Logger */
	private static final Logger LOGGER = Logger.getLogger(TemplateTransformer.class.getName());

	/**
	 * Default constructor
	 */
	public TemplateTransformer() {

	}

	/**
	 * Bind the template and the XML data and save as a word document
	 * 
	 * @param theOutputFilePath
	 *            - the file path to output to
	 * @throws Docx4JException
	 * @throws IOException
	 */
	public void generateDocumentAndSaveAsWord(final byte[] theTemplateWordDocument, final String theXmlData,
			File theOutputFile) throws Docx4JException, IOException {

		ByteArrayInputStream templateStream = null;
		WordprocessingMLPackage wordMLPackage = null;

		try {
			// Load input_template.docx
			final long startTime = System.currentTimeMillis();
			LOGGER.fine("generateDocumentAndSaveAsWord - Loading template");
			templateStream = new ByteArrayInputStream(theTemplateWordDocument);
			wordMLPackage = Docx4J.load(templateStream);
			LOGGER.info("generateDocumentAndSaveAsWord - Loaded template in "
					+ (System.currentTimeMillis() - startTime));
		} finally {
			if (templateStream != null) {
				templateStream.close();
			}
		}

		if (LOGGER.isLoggable(Level.FINER)) {
			LOGGER.finer("generateDocumentAndSaveAsWord - XML Data:\n" + theXmlData);
		}

		LOGGER.fine("generateDocumentAndSaveAsWord - binding xml data");
		Docx4J.bind(wordMLPackage, theXmlData, Docx4J.FLAG_BIND_INSERT_XML & Docx4J.FLAG_BIND_BIND_XML);

		LOGGER.fine("generateDocumentAndSaveAsWord - about to save document");
		// Save the document
		Docx4J.save(wordMLPackage, theOutputFile, Docx4J.FLAG_NONE);

		LOGGER.info("generateDocumentAndSaveAsWord - Saved word document to file: " + theOutputFile.getCanonicalPath());

	}

	/**
	 * Creates a temporary file and writes the generated document contents to this file. Note: the temporary file is
	 * created and set as deleteOnExit.
	 * 
	 * @param theTemplateWordDocument
	 * @param theXmlData
	 * @return
	 * @throws Docx4JException
	 * @throws IOException
	 */
	public File generateDocumentAndSaveAsWord(final byte[] theTemplateWordDocument, final String theXmlData)
			throws Docx4JException, IOException {
		File out = File.createTempFile("docgen", null);
		out.deleteOnExit();
		generateDocumentAndSaveAsWord(theTemplateWordDocument, theXmlData, out);
		return out;
	}

	public String generateDocumentAndReturnInWord(final byte[] theTemplateWordDocument, final String theXmlData)
			throws Docx4JException, IOException {
		File out = File.createTempFile("docgen", null);
		out.deleteOnExit();
		generateDocumentAndSaveAsWord(theTemplateWordDocument, theXmlData, out);
		return FileUtils.readFileToString(out);
	}

	/**
	 * Convenience methid created to be called from IBM BPM. The template will be passed in as abase64 encoded string
	 * and returned as a base 64 encoded string
	 * 
	 * @param theTemplateWordDocument
	 *            - the word document template to be used encoded as a base 64 encoded string
	 * @param theXmlData
	 * @return - the popuated word document template encoded as a base 64 encoded string
	 * @throws Docx4JException
	 * @throws IOException
	 */
	public String generateDocumentAndSaveAsWord(final String theTemplateWordDocument, final String theXmlData)
			throws Docx4JException, IOException {
		File out = null;
		try {
			out = File.createTempFile("docgen", null);

			LOGGER.fine("generateDocumentAndSaveAsWord - received document template of size "
					+ theTemplateWordDocument.length() + " xml data of size " + theXmlData.length());

			long startTime = System.currentTimeMillis();
			byte[] bytes = Base64.decodeBase64(theTemplateWordDocument.getBytes());
			LOGGER.fine("generateDocumentAndSaveAsWord - base64 decoded template in "
					+ (System.currentTimeMillis() - startTime) + " ms");
			generateDocumentAndSaveAsWord(bytes, theXmlData, out);

			startTime = System.currentTimeMillis();
			byte[] responseBytes = FileUtils.readFileToByteArray(out);
			LOGGER.fine("generateDocumentAndSaveAsWord - read generate doc from file in "
					+ (System.currentTimeMillis() - startTime) + " ms");

			startTime = System.currentTimeMillis();
			byte[] retValBytes = Base64.encodeBase64(responseBytes);
			LOGGER.fine("generateDocumentAndSaveAsWord - base64 encoded resultsin "
					+ (System.currentTimeMillis() - startTime) + " ms");

			return new String(retValBytes);
		} finally {
			if (out != null) {
				try {
					boolean isDeleted = out.delete();
					if (!isDeleted) {
						LOGGER.log(Level.WARNING, "Unable to delete file: " + out.getCanonicalPath());
					}
				} catch (IOException ioe) {
					LOGGER.log(Level.WARNING, "Unable to delete file: " + out.getCanonicalPath(), ioe);
				}
			}
		}
	}

	public void generateDocumentAndSaveAsPDF(final byte[] theTemplateWordDocument, final String theXmlData,
			String theOutputFileName) throws IOException, Docx4JException {
		generateDocumentAndSaveAsWord(theTemplateWordDocument, theXmlData, new File(theOutputFileName));
	}

	public void generateDocumentAndSaveAsPDF(final byte[] theTemplateWordDocument, final String theXmlData,
			File theOutputFile) throws IOException, Docx4JException {

		File tempFile = null;
		FileInputStream in = null;
		FileOutputStream out = null;

		try {
			// Create a word document as a temp file
			tempFile = File.createTempFile("transformer", "temp");
			LOGGER.fine("Creating temporary word document prior to PDF conversion into file "
					+ tempFile.getCanonicalPath());

			generateDocumentAndSaveAsWord(theTemplateWordDocument, theXmlData, theOutputFile);

			LOGGER.fine("Loading temporary word document prior to PDF conversion");
			in = new FileInputStream(tempFile);
			XWPFDocument document = new XWPFDocument(in);

			// 2) Prepare Pdf options
			PdfOptions options = PdfOptions.create();

			// 3) Convert XWPFDocument to Pdf
			LOGGER.fine("Converting temporary word document to PDF file " + theOutputFile.getCanonicalPath());
			out = new FileOutputStream(theOutputFile);
			PdfConverter.getInstance().convert(document, out, options);

		} finally {
			if (tempFile != null) {
				try {
					boolean isDeleted = tempFile.delete();
					if (!isDeleted) {
						LOGGER.warning("Unable to delete temporary file: " + tempFile.getCanonicalPath());
					}
				} catch (IOException ioe) {
					LOGGER.log(Level.WARNING, "Unable to delete file: " + tempFile.getCanonicalFile(), ioe);
				}
			}
			if (in != null) {
				try {
					in.close();
				} catch (IOException ioe) {
					// ignore
				}
			}

			if (out != null) {
				try {
					out.close();
				} catch (IOException ioe) {
					// ignore
				}
			}
		}

	}
}
