/**
 * 
 */
package au.com.centrumsystems.docgen;

import java.beans.MethodDescriptor;
import java.beans.ParameterDescriptor;
import java.beans.SimpleBeanInfo;
import java.lang.reflect.Method;

/**
 * @author AlexBullen
 *
 */
public class TemplateTransformerBeanInfo extends SimpleBeanInfo {

	@SuppressWarnings("rawtypes")
	private Class beanClass = TemplateTransformer.class;

	@Override
	public MethodDescriptor[] getMethodDescriptors() {
		try {
			MethodDescriptor methodDescriptor1 = getMethodDescription("generateDocumentAndSaveAsWord", new String[] {
					"theTemplateWordDocument (String)", "theXmlData (String)" }, new Class[] { String.class,
					String.class });
			return new MethodDescriptor[] { methodDescriptor1 };
		} catch (Exception e) {
			return super.getMethodDescriptors();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private MethodDescriptor getMethodDescription(String methodName, String parameters[], Class classes[])
			throws NoSuchMethodException {
		MethodDescriptor methodDescriptor = null;
		Method method = beanClass.getMethod(methodName, classes);

		if (method != null) {
			ParameterDescriptor paramDescriptors[] = new ParameterDescriptor[parameters.length];
			for (int i = 0; i < parameters.length; i++) {
				ParameterDescriptor param = new ParameterDescriptor();
				param.setShortDescription(parameters[i]);
				param.setDisplayName(parameters[i]);
				paramDescriptors[i] = param;
			}
			methodDescriptor = new MethodDescriptor(method, paramDescriptors);
		}

		return methodDescriptor;
	}

}
